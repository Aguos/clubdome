﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class gameManager : MonoBehaviour
{
    public GameObject winText, loseText, endScreen;
    public Text scoreText, HiScoreText, waveAmtText;
    public clubHealth cHealth;
    public clubScore cScore;

    private void Awake()
    {
        cScore = FindObjectOfType<clubScore>();
        cHealth = FindObjectOfType<clubHealth>();
        endScreen.SetActive(false);
        winText.SetActive(false);
        loseText.SetActive(false);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.X))
        {
            if (cHealth.gameOver == true)
            {
                cHealth.enabled = true;
                if (cHealth.levelLost)
                {
                    cHealth.LevelEnd();
                    SceneManager.LoadScene(0);
                }
                if (cHealth.levelWon)
                {
                    cHealth.LevelEnd();
                    SceneManager.LoadScene(0);
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.Z))
        {
            if (cHealth.gameOver == true)
            {
                cHealth.enabled = true;
                if (cHealth.levelWon)
                {
                    cHealth.LevelEnd();
                    endScreen.SetActive(false);
                    cHealth.NextLevel();
                }
                if (cHealth.levelLost)
                {
                    cHealth.LevelEnd();
                    SceneManager.LoadScene(0);
                }
            }
        }

        //if (Input.GetKeyDown(KeyCode.Z))
        //{
        //    if (cHealth.gameOver == true)
        //    {
        //        endScreen.SetActive(false);
        //        cHealth.NextLevel();
        //    }
        //}

    }
}
