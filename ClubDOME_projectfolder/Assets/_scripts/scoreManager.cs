﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scoreManager : MonoBehaviour
{
    public int currentPatronCount, highPatronCount, currentWaveCount, highwaveCount;

    clubManager cManager;
    clubHealth cHealth;
    clubScore cScore;

    private void Awake()
    {
        cManager = FindObjectOfType<clubManager>();
        cHealth = FindObjectOfType<clubHealth>();
        cScore = FindObjectOfType<clubScore>();
    }

    void Start()
    {
        highPatronCount = PlayerPrefs.GetInt("HighScore");
        currentPatronCount = 0;
        currentWaveCount = 1;
    }

    void Update()
    {
        currentPatronCount = cScore.numberOfPatrons;
        PlayerPrefs.SetInt("CurrentScore", currentPatronCount);

        if (currentPatronCount > PlayerPrefs.GetInt("HighScore"))
        {
            PlayerPrefs.SetInt("HighScore", currentPatronCount);
            highPatronCount = currentPatronCount;
        }
        PlayerPrefs.SetInt("HighScore", highPatronCount);

        currentWaveCount = cScore.currentLevel;
        if(currentWaveCount >= highwaveCount)
        {
            PlayerPrefs.SetInt("HighWaveCount", currentWaveCount);
        }
    }

    public void ResetHighScore()
    {
        highPatronCount = 0;
    }

}
