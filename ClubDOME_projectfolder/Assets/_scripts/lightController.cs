﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lightController : MonoBehaviour
{
    clubManager _clubManager;

    public GameObject circle, square, triangle;
    public GameObject[] shapeList = new GameObject[3];

    public float intensityMod = 3;

    public string currentSign;

    void Awake()
    {
        _clubManager = GetComponent<clubManager>(); 
    }

    void Start()
    {
        shapeList[0] = square;
        shapeList[1] = circle;
        shapeList[2] = triangle;
    }

    void Update()
    {
        currentSign = _clubManager.currentTag;

        if (currentSign == "Box")
        {
            shapeList[0].SetActive(true);
            shapeList[1].SetActive(false);
            shapeList[2].SetActive(false);
        }

        if (currentSign == "Pyramid")
        {
            shapeList[0].SetActive(false);
            shapeList[1].SetActive(false);
            shapeList[2].SetActive(true);
        }

        if (currentSign == "Sphere")
        {
            shapeList[0].SetActive(false);
            shapeList[1].SetActive(true);
            shapeList[2].SetActive(false);
        }
    }
}
