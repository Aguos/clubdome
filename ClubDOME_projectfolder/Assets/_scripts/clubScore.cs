﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class clubScore : MonoBehaviour
{
    public int damageAmount = 10, healAmount = 5, currentLevel, numberOfPatrons = 0;

    bool correctType;

    BoxCollider entrance;

    clubHealth cHealth;
    clubManager cManager;
    queueManager qManager;
    soundManager sManager;

    void Awake()
    {
        currentLevel = 1;
        entrance = GetComponent<BoxCollider>();
        cHealth = GetComponent<clubHealth>();
        cManager = GetComponent<clubManager>();
        qManager = GetComponent<queueManager>();
        sManager = GetComponent<soundManager>();

        entrance.isTrigger = true;
    }

    public void OnTriggerEnter(Collider other)
    {
        Renderer currentPatronRenderer = other.gameObject.transform.GetChild(0).gameObject.GetComponent<Renderer>();

        if (other.gameObject.tag != cManager.currentTag)                //This has to check if gameobject is same as shape restriction, not just if its a box. 
        {
            correctType = true;
            
            HealthModifier();
            numberOfPatrons++;
            sManager.clubSounds.clip = sManager.correctSound;
            sManager.clubSounds.Play();
        }

        if (other.gameObject.tag == cManager.currentTag)                //Same as above
        {
            correctType = false;
            HealthModifier();
            sManager.clubSounds.clip = sManager.incorrectSound;
            sManager.clubSounds.Play();
        }

        //Conditions change to color for entry.
        if (currentLevel >= 3)
        {
            if (currentPatronRenderer.material.color == cManager.colorList[cManager.currentSignColor])
            {
                correctType = true;
                HealthModifier();
                Debug.Log("Color Correct");
            }
            else
            {
                correctType = false;
                HealthModifier();
                Debug.Log("Wrong Color");
            }
        }

        //Conditions change to color AND shape for entry.
        if (currentLevel >= 5)
        {
            if (currentPatronRenderer.material.color == cManager.colorList[cManager.currentSignColor] && other.gameObject.tag != cManager.currentTag)
            {
                correctType = true;
                HealthModifier();
                Debug.Log("Color Correct");
            }
            else
            {
                correctType = false;
                HealthModifier();
                Debug.Log("Wrong Color");
            }
        }

    }

    public void HealthModifier()
    {
        if (correctType)
        {
            cHealth.HealDamage(healAmount);
        }

        if (!correctType)
        {
            cHealth.TakeDamage(damageAmount);
        }
    }
}
