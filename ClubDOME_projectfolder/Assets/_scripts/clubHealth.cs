﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class clubHealth : MonoBehaviour
{
    public float startingHP = 50f, dotAmount = 1f, currentHP, maxHP = 100f;
    public Slider healthSlider;

    public bool gameOver, isDead, levelWon, levelLost;

    clubManager cManager;
    clubHealth cHealth;
    queueManager qManager;
    gameManager gManager;
    scoreManager sManager;
    clubScore cScore;

    void Awake()
    {
        levelWon = false;
        levelLost = false;
        currentHP = startingHP;
        maxHP = 100;
        cManager = GetComponent<clubManager>();
        qManager = GetComponent<queueManager>();
        cHealth = GetComponent<clubHealth>();
        cScore = GetComponent<clubScore>();
        gManager = GameObject.Find("Game Manager").GetComponent<gameManager>();
        sManager = FindObjectOfType<scoreManager>();
    }

    void Update()
    {
        currentHP -= (dotAmount * Time.deltaTime);
        healthSlider.value = currentHP;

        if (currentHP <= 0 && !isDead)
        {
            Death();
        }
        return;
    }

    public void TakeDamage(int amount)
    {
        currentHP -= amount;
        healthSlider.value = currentHP;

        if (currentHP <= 0 && !isDead)
        {
            Death();
        }
        return;
    }

    public void HealDamage(int amount)
    {
        currentHP += amount;
        healthSlider.value = currentHP;

        if (currentHP >= maxHP && !isDead)
        {
            Win();
        }
        return;
    }

    private void Death()
    {
        isDead = true;
        cManager.enabled = false;
        qManager.enabled = false;
        cHealth.enabled = false;
        gManager.loseText.SetActive(true);
        gameOver = true;
        levelLost = true;
        LevelEnd(); 
    }

    private void Win()
    {
        cManager.enabled = false;
        qManager.enabled = false;
        cHealth.enabled = false;   
        gManager.winText.SetActive(true);
        gameOver = true;
        levelWon = true;
        LevelEnd();
    }

    public void LevelEnd()
    {
        if (gameOver == true)
        {
            gManager.enabled = true;
            gManager.endScreen.SetActive(true);
        }

        gManager.scoreText.text = PlayerPrefs.GetInt("CurrentScore").ToString();
        gManager.HiScoreText.text = PlayerPrefs.GetInt("HighScore").ToString();
        gManager.waveAmtText.text = cScore.currentLevel.ToString();
    }

    //Start of level goal calculation.

    public void NextLevel()
    {
        cScore.currentLevel++;
        print("Starting level " + cScore.currentLevel);
        maxHP = maxHP * 1.5f;
        currentHP = maxHP / 2;
        healthSlider.value = currentHP;
        healthSlider.maxValue = maxHP;
        EnableElements();
    }

    public void EnableElements()
    {
        cManager.enabled = true;
        gameOver = false;
        isDead = false;
        qManager.enabled = true;
        this.enabled = true;
        levelWon = false;
        levelLost = false;
    }
}
