﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class queueManager : MonoBehaviour
{
    clubManager _clubManager;

    private void Awake()
    {
        _clubManager = GetComponent<clubManager>();     
    }

    void Start()
    {
        CreateNewPatron();
    }

    void Update()
    {
        if (_clubManager.currentPatron == null)
        {
            CreateNewPatron();
        }
    }

    void CreateNewPatron()
    {
        _clubManager.currentPatron = Instantiate(_clubManager.shapeList[Random.Range(0, _clubManager.shapeList.Length)], _clubManager.pos1.position, _clubManager.pos1.rotation);
        _clubManager.ChangeMaterial();
    }
}
