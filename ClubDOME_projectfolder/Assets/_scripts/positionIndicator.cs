﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class positionIndicator : MonoBehaviour
{
    private Transform interactionTransform;

    void OnDrawGizmos()
    {
        if (interactionTransform == null)
        {
            interactionTransform = transform;
        }

        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(interactionTransform.position, 1);
    }
}
