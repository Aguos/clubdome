﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bouncerAnim : MonoBehaviour
{
    Animator bouncer;
    clubManager _clubManager;

    void Start()
    {
        bouncer = GetComponent<Animator>();
        _clubManager = GetComponentInParent<clubManager>();
    }

    void Update()
    {
        isAccepted();
        isRejected();
    }

    void isRejected()
    {
        if (_clubManager.isRejected)
        {
            bouncer.SetBool("isRejected", true);
        }
        else
        {
            bouncer.SetBool("isRejected", false);
        }
    }

    void isAccepted()
    {  
        if (_clubManager.isAccepted)
        {
            bouncer.SetBool("isAccepted", true);
        }
        else
        {
            bouncer.SetBool("isAccepted", false);
        }
    }
}
