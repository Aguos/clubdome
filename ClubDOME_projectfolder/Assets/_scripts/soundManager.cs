﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class soundManager : MonoBehaviour
{
    public AudioClip correctSound, incorrectSound;
    public AudioSource clubSounds;

    void Awake()
    {
        clubSounds = GetComponent<AudioSource>();
    }
}
