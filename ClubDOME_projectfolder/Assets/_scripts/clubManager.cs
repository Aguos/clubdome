﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class clubManager : MonoBehaviour
{
    public float tagChangeTime = 5f, speed = 5f, startTime;
    private float distLength;
    public int currentMat, currentSignColor;
    public string currentTag;

    #region Hidden In Inspector
    [HideInInspector]
    public Material currentMaterial;
    [HideInInspector]
    public string[] tagList = new string[3];
    [HideInInspector]
    public Color[] colorList = new Color[3], emissionColorList = new Color[3], eSignColorList = new Color[3];
    #endregion

    public GameObject currentPatron, clubSign, currentSignShape;
    public GameObject[] shapeList;
    public lightController lController;

    public Transform pos1, acceptPos, rejectPos;

    bool isMoving = false;
    public bool isAccepted = false, isRejected = false;

    Animator bouncerAnim;

    private void Awake()
    {
        tagList[0] = "Box";
        tagList[1] = "Sphere";
        tagList[2] = "Pyramid";
        colorList[0] = new Color(1.0f, 0.894f, 0.0f);
        colorList[1] = new Color(0, 1, .15f);
        colorList[2] = new Color(0, .37f, .62f);
        emissionColorList[0] = new Color(.642f, .25f, .11f);
        emissionColorList[1] = new Color(.18f, .62f, .23f);
        emissionColorList[2] = new Color(.23f, .62f, .77f);
        eSignColorList[0] = new Color();
        eSignColorList[1] = new Color();
        eSignColorList[2] = new Color();

        lController = FindObjectOfType<lightController>();
    }
    void Start()
    {
        currentMat = Random.Range(0, colorList.Length);
        currentTag = tagList[Random.Range(0, tagList.Length)];
        currentSignShape = clubSign.transform.GetChild(Random.Range(0, clubSign.transform.childCount)).gameObject;

        StartCoroutine(IChangeSign());
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Z))
        {
            if(isMoving == false)
            {
                startTime = Time.time;
                isAccepted = true;
            }
        }

        if (Input.GetKeyDown(KeyCode.X))
        {
            if (isMoving == false)
            {
                startTime = Time.time;
                isRejected = true;
            }
        }

        if (isAccepted)
        {           
            isMoving = true;
            SendPatron(acceptPos);
        }

        if (isRejected)
        {
            isMoving = true;
            SendPatron(rejectPos);
        }
    }

    public void SendPatron(Transform posToSend)
    {
        distLength = Vector3.Distance(pos1.position, posToSend.position);
        float distCovered = (Time.time - startTime) * speed;
        float distStep = distCovered / distLength;
        currentPatron.transform.position = Vector3.Lerp(pos1.position, posToSend.position, distStep);
            if (currentPatron.transform.position == posToSend.position)
            {
                isRejected = false;
                isAccepted = false;
                isMoving = false;
                Destroy(currentPatron);
            }
    }

    void ChangeTag()
    {
        currentTag = tagList[Random.Range(0, tagList.Length)];
    }

    void ChangeSignColor()
    {
        for (int i = 0; i < clubSign.transform.childCount; i++)
        {
            if (clubSign.transform.GetChild(i).gameObject.activeSelf == true)
            {
                currentSignShape = clubSign.transform.GetChild(i).gameObject;
                //Debug.Log("Current Sign: " + currentSignShape);
            }
        }
        currentSignColor = Random.Range(0, colorList.Length);
        Renderer signRend = currentSignShape.GetComponent<Renderer>();
        signRend.material.color = colorList[currentSignColor];
        signRend.material.SetColor("_EmissionColor", (emissionColorList[currentSignColor]) * 3.0f);
    }

    public void ChangeMaterial()
    {
        Renderer rendererHead = currentPatron.GetComponentInChildren<Renderer>();
        Renderer rendererBody = currentPatron.GetComponentInChildren<Renderer>();

        rendererHead = currentPatron.transform.GetChild(0).GetComponent<Renderer>();
        rendererBody = currentPatron.transform.GetChild(1).GetComponent<Renderer>();

        rendererHead.material.color = colorList[currentMat];
        rendererHead.material.SetColor("_EmissionColor", emissionColorList[currentMat]);

        rendererBody.material.color = colorList[currentMat];
        rendererBody.material.SetColor("_EmissionColor", emissionColorList[currentMat]);
    }

    public IEnumerator IChangeSign()
    {
            InvokeRepeating("ChangeTag", tagChangeTime, tagChangeTime);
            InvokeRepeating("ChangeSignColor", tagChangeTime, tagChangeTime);
        yield return null;
    }
}
